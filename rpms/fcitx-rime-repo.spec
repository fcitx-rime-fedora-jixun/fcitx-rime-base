Name:       fcitx-rime-jixun-release
Version:    %{fedora}
Release:    0.1
Summary:    fcitx-rime unofficial repo by (jixun.moe)
Summary(zh_CN): fcitx-rime 第三方源 (jixun.moe)
License:    MIT
Group:      System Environment/Base
URL:        https://gitlab.com/fcitx-rime-fedora-jixun
BuildArch:  noarch

%description
fcitx-rime unofficial repo by (jixun.moe)

%prep

%build

%install
install -d -m 755 %{buildroot}/etc/yum.repos.d

cat > %{buildroot}/etc/yum.repos.d/fcitx-rime-jixun.repo <<EOF
# Unofficial fcitx-rime repo for fedora 29 and later?

[fcitx-rime-jixun]
name=fcitx-rime fc\$releasever
baseurl=https://fcitx-rime-fedora-jixun.gitlab.io/fcitx-rime-fc\$releasever/RPMS
skip_if_unavailable=True
metadata_expire=7d
gpgcheck=0
enabled=1

[fcitx-rime-jixun-source]
name=fcitx-rime fc\$releasever - source
baseurl=https://fcitx-rime-fedora-jixun.gitlab.io/fcitx-rime-fc\$releasever/SRPMS
skip_if_unavailable=True
metadata_expire=7d
gpgcheck=0
enabled=0
EOF

%files
%defattr(-,root,root,-)
%dir /etc/yum.repos.d
%config(noreplace) /etc/yum.repos.d/fcitx-rime-jixun*.repo

%changelog
* Thu May 02 2019 jixun <jixun.moe@gmail.com> - 30-0.1
- Initial build