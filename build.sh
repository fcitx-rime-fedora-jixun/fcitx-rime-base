#!/usr/bin/env bash

function prepare_src()
{
  spectool -g -R ${SPECPATH}
  find ${SPECDIR} -type f -not -iname "*.spec" -exec cp {} ${BUILD_DIR}/SOURCES/ \;
}

function rpm_build()
{
  # log_path=$(echo ${SPECPATH} | sed 's/\//_/g' | sed 's/ /_/g')
  # echo "* yum-builddep for ${PKGNAME}"
  # sudo yum-builddep -y ${SPECPATH} >/dev/null
  echo "* rpmbuild for ${PKGNAME}"
  rpmbuild -ba ${SPECPATH}
}

function traverse_build()
{
  TOP_DIR=$1
  for i in `find ${TOP_DIR} -type f -iname "*.spec"` ;
  do
    echo $SPECPATH
    SPECPATH=$i
    SPECNAME=$(echo ${SPECPATH} | awk -F '/' '{print $NF}')
    SPECDIR=$(echo ${SPECPATH} | sed 's/${SPECNAME}//g')
    PKGNAME=$(echo ${SPECNAME} | sed 's/.SPEC//g' | sed 's/.spec//g')

    prepare_src
    rpm_build
  done
}

export BUILD_DIR=$HOME/rpmbuild
traverse_build $@

createrepo --update ${BUILD_DIR}/RPMS
createrepo --update ${BUILD_DIR}/SRPMS

mkdir -p public
rm -rf public/*
cp -r ${BUILD_DIR}/RPMS  public
cp -r ${BUILD_DIR}/SRPMS public
